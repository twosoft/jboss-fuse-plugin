package cl.twosoft.jenkins.plugin.fuse;

import java.util.List;
import java.util.Map;

/**
 * Twosoft 2018
 * @author Cristian Saez V.
 *
 */
public class ProfileEdit {

	private static final Logger LOG = Logger.factory(ProfileEdit.class);

	/**
	 * @author Cristian Saez V.
	 * @param template
	 * @param arguments
	 */
	public ProfileEdit() {

	}

	/**
	 * @author Cristian Saez V.
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> bundleExist() throws Exception {

		String cmd = this.cmdDisplay();
		cmd = cmd + " " + this.cmdFindBundle();

		Shell shell = new Shell();
		shell.execFuse(cmd);

		String console = shell.console();

		Bundle artifactUtil = new Bundle();
		List<Map<String, String>> artifacts = artifactUtil.parserMix(console);

		String id;
		String group;
		for (Map<String, String> artifact : artifacts) {

			id = artifact.get(EnvVar.ARTIFACT_ID);
			group = artifact.get(EnvVar.GROUP_ID);

			if (Options.getArtifactId().equals(id) && Options.getGroupId().equals(group)) {
				return artifact;
			}
		}

		return null;
	}

	public void bundleDelete(Map<String, String> bundle) throws Exception {

		String cmdBundleDelete = this.cmdEdit(bundle, true);

		try {

			Shell shell = new Shell();
			shell.execFuse(cmdBundleDelete);

		} catch (Exception e) {
			LOG.error("Error al tratar de eliminar el bundle del perfil");
			LOG.error(" - " + e.getMessage());
			throw e;
		}

	}

	public String cmdBundleInstall() {

		Bundle bundle = new Bundle();
		Map<String, String> bundleMap = bundle.getBundleMap();
		String cmdEdit = this.cmdEdit(bundleMap, false);

		return cmdEdit;
	}

	/**
	 * @author Cristian Saez V.
	 * @return
	 */
	private String cmdDisplay() {

		String cmd = PropertyApp.get(PropertyApp.PROFILE_DISPLAY);
		cmd = cmd.replaceAll(EnvVar.token(EnvVar.PROFILE_BUNDLE), Options.getProfileBundle());
		return cmd;

	}

	/**
	 * @author Cristian Saez V.
	 * @return
	 */
	private String cmdFindBundle() {

		String bundleFind = PropertyApp.get(PropertyApp.PROFILE_DISPLAY_BUNDLE);
		bundleFind = bundleFind.replaceAll(EnvVar.token(EnvVar.ARTIFACT_ID), Options.getArtifactId())
				.replaceAll(EnvVar.token(EnvVar.GROUP_ID), Options.getGroupId());

		return bundleFind;
	}

	/**
	 * @author Cristian Saez V.
	 * @param artifact
	 * @return
	 */
	private String cmdEdit(Map<String, String> artifact, boolean delete) {

		Bundle artifactUtil = new Bundle();
		String mvnArtifact = artifactUtil.toMaven(artifact);
		
		String cmdProfileDeleteBundle = PropertyApp.get(PropertyApp.PROFILE_EDIT_BUNDLE);
		cmdProfileDeleteBundle = cmdProfileDeleteBundle.replaceAll(EnvVar.token(EnvVar.BUNDLE), mvnArtifact)
				.replaceAll(EnvVar.token(EnvVar.PROFILE_BUNDLE), Options.getProfileBundle());

		cmdProfileDeleteBundle = this.cmdBundleDelete(cmdProfileDeleteBundle, delete);

		return cmdProfileDeleteBundle;

	}

	/**
	 * @author Cristian Saez V.
	 * @param cmd
	 * @param delete
	 * @return
	 */
	private String cmdBundleDelete(String cmd, boolean delete) {

		String del = (delete ? "--delete" : "");
		return cmd.replaceAll(EnvVar.token(EnvVar.DELETE), del);

	}

}
