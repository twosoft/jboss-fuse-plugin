package cl.twosoft.jenkins.plugin.fuse;

import java.io.InputStream;
import java.util.Properties;

/**
 * Twosoft
 * @author Cristian Saez V.
 *
 */
public final class PropertyApp {

	private static final Logger LOG = Logger.factory(PropertyApp.class);

	private static Properties properties;

	private PropertyApp() {

	}

	/**
	 * @author Cristian Saez V.
	 * @throws Exception
	 */
	public static final void load(Class<?> clazz) throws Exception {

		try {

			LOG.debug("Cargando las propiedades del plugin");

			InputStream url = clazz.getClassLoader().getResourceAsStream(TEMPLATE_PROPERTIES);
			PropertyApp.properties = new Properties();
			PropertyApp.properties.load(url);

			LOG.debug("Propiedades cargadas");

		} catch (Exception e) {
			LOG.error("Error al cargar las propiedades del plugin");
			LOG.error(" - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * @author Cristian Saez V.
	 * @param key
	 * @return
	 */
	public static final String get(String key) {
		return PropertyApp.properties.getProperty(key);
	}

	/**
	 * Plugin
	 */
	public static final String TEMPLATE_PROPERTIES = "templates.properties";
	
	/**
	 * Fuse
	 */
	public static final String PROFILE_EDIT_PROPERTIES = "fuse.cmd.profile.edit.properties";
	public static final String PROFILE_EDIT_BUNDLE = "fuse.cmd.profile.edit.bundle";
	public static final String PROFILE_REFRESH = "fuse.cmd.profile.refresh";
	public static final String PROFILE_DISPLAY = "fuse.cmd.profile.display";
	public static final String PROFILE_DISPLAY_BUNDLE = "fuse.cmd.profile.display.bundle";
	
	public static final String FUSE_CLIENT_HOME = "fuse.client.home";
	
	public static final String SHELL_FUSE_CLIENT = "shell.cmd.fuse.client";
	public static final String SHELL_FUSE_EXEC = "shell.cmd.fuse.exec";
	
	public static final String FUSE_CREDENTIAL = "fuse.credential";
	
	public static final String LOGGER_LEVEL = "plugin.logger.level";
	
	
	/**
	 * Fuse Credential
	 */
	public static final String ENV_HOST = "host";
	public static final String ENV_USER = "user";
	public static final String ENV_PASS = "password";
	public static final String ENV_PORT = "port";

}
