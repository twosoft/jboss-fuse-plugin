package cl.twosoft.jenkins.plugin.fuse;

/**
 * Twosoft 2018
 * @author Cristian Saez V.
 *
 */
public final class Options {

	private static String artifactId;
	private static String groupId;
	private static String version;
	private static String profileBundle;
	private static String profileProperties;
	private static String workspace;
	private static String ambiente;
	private static String fuser;
	private static String fpass;

	private Options() {

	}

	public static final String getArtifactId() {
		return artifactId;
	}

	public static final String getGroupId() {
		return groupId;
	}

	public static final String getVersion() {
		return version;
	}

	public static final String getProfileBundle() {
		return profileBundle;
	}

	public static final String getProfileProperties() {
		return profileProperties;
	}

	public static final String getWorkspace() {
		return workspace;
	}

	public static final String getAmbiente() {
		return ambiente;
	}
	
	public static final String getFUser() {
		return fuser;
	}
	
	public static final String getFPass() {
		return fpass;
	}

	private static final String ARTIFACT_ID = "-artifactId";
	private static final String ARTIFACT_GROUP = "-groupId";
	private static final String ARTIFACT_VERSION = "-version";
	private static final String PROFILE_BUNDLE = "-profileBundle";
	private static final String PROFILE_PROPERTIES = "-profileProperties";
	private static final String WORKSPACE = "-workspace";
	private static final String FUSE_USER = "-fuser";
	private static final String FUSE_PASS = "-fpass";
	public static final String AMBIENTE = "-ambiente";

	public static final void load(String[] args) {
		String op;
		for (int i = 0; i < args.length; i++) {
			op = args[i];
			if (ARTIFACT_ID.equals(op)) {
				i++;
				artifactId = args[i];
				continue;
			} else if (ARTIFACT_GROUP.equals(op)) {
				i++;
				groupId = args[i];
				continue;
			} else if (ARTIFACT_VERSION.equals(op)) {
				i++;
				version = args[i];
				continue;
			} else if (PROFILE_BUNDLE.equals(op)) {
				i++;
				profileBundle = args[i];
				continue;
			} else if (PROFILE_PROPERTIES.equals(op)) {
				i++;
				profileProperties = args[i];
				continue;
			} else if (WORKSPACE.equals(op)) {
				i++;
				workspace = args[i];
				continue;
			} else if (AMBIENTE.equals(op)) {
				i++;
				ambiente = args[i];
				continue;
			} else if (FUSE_USER.equals(op)) {
				i++;
				fuser = args[i];
				continue;
			} else if (FUSE_PASS.equals(op)) {
				i++;
				fpass = args[i];
				continue;
			}
		}
	}

	public static final String values() {
		return "Options [artifactId=" + artifactId + ", groupId=" + groupId + ", version=" + version
				+ ", profileBundle=" + profileBundle + ", profileProperties=" + profileProperties + ", workspace="
				+ workspace + ", ambiente=" + ambiente + "]";
	}

}
