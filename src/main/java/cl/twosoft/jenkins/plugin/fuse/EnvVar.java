package cl.twosoft.jenkins.plugin.fuse;

/**
 * Twosoft 2018
 * @author Cristian Saez V.
 *
 */
public final class EnvVar {

	public static final String ARTIFACT_ID = "artifactId";
	public static final String GROUP_ID = "groupId";
	public static final String VERSION = "version";
	
	public static final String PROFILE_PROPERTIES = "profileProperties";
	public static final String PROFILE_BUNDLE = "profileBundle";
	
	public static final String PROPERTY_NAME = "propertyName";
	public static final String PROPERTY_VALUE = "propertyValue";

	public static final String FUSE_SCRIPT = "scriptFuse";
	public static final String FUSE_CNX = "cnx";
	
	public static final String SCRIPT_CLIENT = "client";

	public static final String TOKEN_FIRST = ":";
	public static final String TOKEN_LATEST = "";

	public static final String FUSE_USER = "user";
	public static final String FUSE_PASSWORD = "password";
	public static final String FUSE_HOST = "host";
	public static final String FUSE_CLIENT_SH = "client";
	
	public static final String BUNDLE = "bundle";
	public static final String DELETE = "delete";
	
	public static final String token(String var) {
		return EnvVar.TOKEN_FIRST + var + EnvVar.TOKEN_LATEST;
	}

}

