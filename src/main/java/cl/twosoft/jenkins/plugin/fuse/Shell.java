package cl.twosoft.jenkins.plugin.fuse;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Twosoft
 * @author Cristian Saez V.
 *
 */
public class Shell {

	private static final Logger LOG = Logger.factory(Shell.class);

	private StringBuffer console;

	/**
	 * @author Cristian Saez V.
	 * 
	 */
	public Shell() {
		this.console = new StringBuffer();
	}

	/**
	 * @author Cristian Saez V.
	 * @param cmd
	 * @throws Exception
	 */
	public void exec(String cmd) throws Exception {
		try {

			this.console = new StringBuffer();
			Process process = Runtime.getRuntime().exec(cmd);
			InputStream inputStream = process.getInputStream();
			BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));

			String line;
			while ((line = buffer.readLine()) != null) {
				LOG.info(line);
				this.console.append(line);
			}

		} catch (Exception e) {
			LOG.error("Error al ejecutar las ordenes");
			LOG.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * @author Cristian Saez V.
	 * @param cmdFuse
	 * @throws Exception
	 */
	public void execFuse(String cmdFuse) throws Exception {

		FileUtil fileUtil = new FileUtil();
		String cmdFusePath = fileUtil.write(cmdFuse);

		FuseConexion fuseConexion = new FuseConexion();
		String cmdCnx = fuseConexion.get();

		String clientCmdTemplate = PropertyApp.get(PropertyApp.SHELL_FUSE_EXEC);

		String cmd = clientCmdTemplate.replaceAll(EnvVar.token(EnvVar.FUSE_CNX), cmdCnx)
				.replaceAll(EnvVar.token(EnvVar.FUSE_SCRIPT), cmdFusePath);

		this.exec(cmd);

	}

	/**
	 * @author Cristian Saez V.
	 * @return
	 */
	public String console() {
		return this.console.toString();
	}

	public void execFuse(String... cmds) throws Exception {
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < cmds.length; i++) {
			stringBuffer.append(cmds[i]).append("\n");
		}

		this.execFuse(stringBuffer.toString());
		return;
	}

}
