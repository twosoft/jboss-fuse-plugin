
# Plugin Jenkins/JBoss Fuse
==============================

## Descripcion
Este es un plug-in que ayuda a la integración entre Jenkins y Jboss Fuse, su desarrollo se debe a la necesidad de simplificar la comunicación, instalación y configuración de bundles de distintos servicios, automatizando y estandarizando en flujo de integración continua.
Este plugin cuenta con las siguientes capacidades:
+ Instalacion de bundles en un perfil especifico en Jboss Fuse a través de Maven (Nexus).
+ Creacion de las properties del servicio en el perfil especificado.
+ Actualización de bundles distintas o iguales versiones (elimina otras versiones instaladas previamente en Fuse).

`Bitbucket -> Jenkins -> Nexus -> Jenkins.Plugin -> Fuse`

## Archivos
+ `/src/main/resoruces/credential-fuse.properties`: Este archivo de propiedades, debe contener las credenciales para abrir una conexión a un servidor Jboss Fuse, se pueden configurar tantas conexiones como sea necesario, para que el Plug-in pueda desplegar en el ambiente que se necesite. El formato de estas properties es el siguiente:

```
fuse.[nombre_ambiente].host
fuse.[nombre_ambiente].user
fuse.[nombre_ambiente].password
```

Ejemplo:
```
fuse.desarrollo.host=localhost
fuse.desarrollo.user=admin
fuse.desarrollo.password=admin
```
> Nota: El valor de [nombre_ambiente] sera el parametro de entrada `-ambiente` al momento de ejecutar el plugin.

Este archivos de propiedades, por un tema de seguridad, debe estar fuera del plug-in, en alguna ruta del sistema operativo donde el usuario jenkins pueda tener acceso de lectura, esta ruta se configura en la propiedad `fuse.credential` que se encuentra en el archivo de propiedades del plug-in.

+ `/src/main/resoruces/templates.properties`: Este archivo contiene los templates de los comandos de ejecución, ruta al archivo de credenciales, ejecutables, etc.

```
fuse.cmd.profile.edit.properties=fabric:profile-edit --pid :artifactId/:propertyName=:propertyValue :profileProperties
shell.cmd.fuse.exec=:cnx -f :scriptFuse
fuse.cmd.profile.edit.bundle=fabric:profile-edit :delete --bundle :bundle :profileBundle
fuse.cmd.profile.display=profile-display :profileBundle
fuse.cmd.profile.display.bundle=|grep ':groupId/:artifactId'
shell.cmd.fuse.client=sh :client -h :host -u :user -p :password
fuse.cmd.profile.refresh=fabric:profile-refresh :profileBundle

# Home JBoss Fuse
fuse.client.home=$JBOSS_HOME

# Logger (INFO/DEBUG)
plugin.logger.level=INFO

# Credenciales Ambientes
fuse.credential=$JENKINS_HOME/credential-fuse.properties
```

### Dependencias
No tiene.

## Instalación
El plug-in se puede configurar/instalar de la siguiente forma:
1. Antes de compilar el plugin debe configurar en el archivo `template.properties` los valores de `fuse.cliente.home` y `fuse.credential`
2. Instalar en Jenkins el jar creado, de preferencia el plugin deberia quedar bajo la ruta de $JENKINS_HOME.

## Configurar un Job en Jenkins
Para que un job de Jenkins pueda utilizar el plug-in no se debe realizar ninguna configuración adicional, con excepción de variables de entorno en Jenkins que puedan abreviar o hacer mas dinamica la interacción del plugin con Jenkins. Al momento de configurar el job, en la sección "Pasos posteriores"->"Ejecutar linea de comandos (shell)" (Jenkins), los parametros de entrada requeridos por el plug-in son los siguentes:
```
-artifactId				: Id del artefacto a desplegar.
-groupId				: Identificador del grupo del artefacto.
-version				: Version del artefacto
-profileBundle			: Perfil en el cual se debe desplegar el artefacto dentro de JBoss Fuse
-profileProperties		: Perfil en el cual se deben cargar las propiedades del artefacto
-workspace				: Espacio de trabajao de Jenkins.
-fuser					: Usuario JBoss Fuse (alternativo al parametro credential-fuse.properties/fuse.[ambiente].user)
-fpass					: Password de JBoss Fuse. (alternativo al archivo credential-fuse.properties/fuse.[ambiente].password)
```
Los parametros de entrada del plugin en su mayoria son fácilmente configurables ya que Jenkins puede proporcionarlos de forma dinamica atraves de variables de entorno como se muestra en el siguente ejemplo:
```
java -jar $JENKINS_HOME/plugin-jenkins-fuse-1.0.0.jar -artifactId $POM_ARTIFACTID -groupId $POM_GROUPID -version $POM_VERSION -profileBundle develop -profileProperties develop -workspace $WORKSPACE -ambiente desarrollo -fuser admin -fpass admin
```
> Notas
Para que el plugin pueda leer las propiedades del bundle y crearlas en Fuse, estas deben estar en un archivo .properties ubicado dentro del proyecto, en la ruta  `${project}/files/properties/[artifactId].properties`

## Bugs
Al proyecto no se le a desarrollado un plan de pruebas y/o comportamento por lo cual puede presentar errores en algunas condiciones.


Creado por: Cristian Sáez V.
Twosoft, Software Developer 2018

@ COPYLEFT, All errors reserved.
@ LGPL Lesser General Public License - https://www.gnu.org/licenses/lgpl-3.0.en.html
@ Santiago, Chile 2018

