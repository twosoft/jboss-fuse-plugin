package cl.twosoft.jenkins.plugin.fuse;

import java.util.Properties;

/**
 * Twosoft 2018
 * @author Cristian Saez V.
 *
 */
public class FuseConexion {

	private static final Logger LOG = Logger.factory(FuseConexion.class);

	private Properties credenciales;
	private String ambiente;

	/**
	 * @author Cristian Saez V.
	 * @throws Exception
	 */
	public FuseConexion() throws Exception {
		this.ambiente = Options.getAmbiente();
	}

	/**
	 * @author Cristian Saez V.
	 * @return
	 * @throws Exception
	 */
	public String get() throws Exception {
		this.credenciales = loadCredentials();
		String cmdCnx = this.cmd();
		return cmdCnx;
	}

	/**
	 * @author Cristian Saez V.
	 * @return
	 * @throws Exception
	 */
	private Properties loadCredentials() throws Exception {

		String path = PropertyApp.get(PropertyApp.FUSE_CREDENTIAL);
		FileUtil fileUtil = new FileUtil();
		Properties credenciales = fileUtil.localProperties(path);
		return this.setCredentialParam(credenciales);

	}
	
	/**
	 * @author Cristian Saez V.
	 * @param properties
	 * @return
	 */
	private Properties setCredentialParam(Properties properties) {
		if (Options.getFUser() != null && Options.getFPass() != null) {
			properties.setProperty(this.getKeyProperty(EnvVar.FUSE_USER), Options.getFUser());
			properties.setProperty(this.getKeyProperty(EnvVar.FUSE_PASSWORD), Options.getFPass());
		}
		return properties;
	}

	/**
	 * @author Cristian Saez V.
	 * @return
	 * @throws Exception 
	 */
	private String cmd() throws Exception {

		try {
			
			String host = this.getProp(PropertyApp.ENV_HOST);
			String user = this.getProp(PropertyApp.ENV_USER);
			String pass = this.getProp(PropertyApp.ENV_PASS);

			String cmdCnx = PropertyApp.get(PropertyApp.SHELL_FUSE_CLIENT);
			String shClient = PropertyApp.get(PropertyApp.FUSE_CLIENT_HOME);
			shClient += "/bin/client";

			cmdCnx = cmdCnx.replaceAll(EnvVar.token(EnvVar.FUSE_CLIENT_SH), shClient)
					.replaceAll(EnvVar.token(EnvVar.FUSE_USER), user)
					.replaceAll(EnvVar.token(EnvVar.FUSE_PASSWORD), pass)
					.replaceAll(EnvVar.token(EnvVar.FUSE_HOST), host);

			return cmdCnx;
			
		} catch (Exception e) {
			LOG.error("No fue posible leer las credenciales de Fuse.");
			throw e;
		}

	}

	/**
	 * @author Cristian Saez V.
	 * @param prop
	 * @return
	 */
	private String getProp(String prop) {
		return this.credenciales.getProperty(this.getKeyProperty(prop));
	}

	private String getKeyProperty(String name) {
		return "fuse." + this.ambiente + "." + name;
	}

	/**
	 * 
	 * @author Cristian Saez V.
	 *
	 */
//	public final class ENVIRONMENT {
//
//		public static final String DEVELOPMENT = "desarrollo";
//		public static final String PRODUCTION = "produccion";
//
//	}

}
