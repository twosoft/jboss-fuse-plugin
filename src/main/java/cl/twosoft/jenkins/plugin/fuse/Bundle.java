package cl.twosoft.jenkins.plugin.fuse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Twosoft 2018
 * @author Cristian Saez V.
 *
 */
public class Bundle {

	private static final Logger LOG = Logger.factory(Bundle.class);

	public Bundle() {
	}

	/**
	 * @author Cristian Saez V.
	 * @param artifact
	 * @return
	 */
	public Map<String, String> parser(String artifact) {

		String[] artifactArray = this.toArray(artifact);

		if (artifactArray.length != 3) {
			LOG.debug("No fue posible determinar el artefacto: " + artifact);
			return null;
		}

		Map<String, String> artifactMap = new HashMap<String, String>();
		artifactMap.put(EnvVar.GROUP_ID, artifactArray[0]);
		artifactMap.put(EnvVar.ARTIFACT_ID, artifactArray[1]);
		artifactMap.put(EnvVar.VERSION, artifactArray[2]);

		return artifactMap;
	}

	/**
	 * @author Cristian Saez V.
	 * @param artifactsBuffer
	 * @return
	 */
	public List<Map<String, String>> parserMix(String artifactsBuffer) {

		List<Map<String, String>> artifacts = new ArrayList<Map<String, String>>();

		String[] lines = artifactsBuffer.split("\t");
		Map<String, String> artifact;
		for (int i = 0; i < lines.length; i++) {
			artifact = this.parser(lines[i]);
			if (artifact == null)
				continue;
			else
				artifacts.add(artifact);
		}
		return artifacts;
	}

	/**
	 * @author Cristian Saez V.
	 * @param artifact
	 * @return
	 */
	private String[] toArray(String artifact) {

		artifact = artifact.replaceAll("\t", "");
		String[] attr = artifact.split("/");

		attr[0] = attr[0].replaceAll("mvn:", "");
		
		if (attr.length == 3) {
			String[] version = attr[2].split(";");
			if (version.length == 2) {
				attr[2] = version[0];
			}
		}
		
		return attr;
	}

	/**
	 * @author Cristian Saez V.
	 * @param artifact
	 * @return
	 */
	public String toMaven(Map<String, String> artifact) {

		String id = artifact.get(EnvVar.ARTIFACT_ID);
		String group = artifact.get(EnvVar.GROUP_ID);
		String version = artifact.get(EnvVar.VERSION);

		return this.toMaven(group, id, version);

	}

	/**
	 * @author Cristian Saez V.
	 * @param group
	 * @param artifact
	 * @param version
	 * @return
	 */
	private String toMaven(String group, String artifact, String version) {

		StringBuffer bundle = new StringBuffer();
		bundle.append("mvn:");
		bundle.append(group).append("/");
		bundle.append(artifact).append("/");
		bundle.append(version);

		return bundle.toString();
	}

	/**
	 * 
	 * @author Cristian Saez V.
	 * @return
	 */
	public String getBundleMaven() {
		return this.toMaven(Options.getGroupId(), Options.getArtifactId(), Options.getVersion());
	}

	/**
	 * 
	 * @author Cristian Saez V.
	 * @return
	 */
	public Map<String, String> getBundleMap() {
		String bundleMvn = this.getBundleMaven();
		return this.parser(bundleMvn);
	}

}
