package cl.twosoft.jenkins.plugin.fuse;

import java.util.Map;

/**
 * Twosoft 2018
 * @author Cristian Saez V.
 *
 */
public class Install {

	private static Logger LOG = Logger.factory(Install.class);

	public static void main(String[] args) {

		try {
			
			Install install = new Install();
			PropertyApp.load(install.getClass());
			Logger.setLevel(PropertyApp.LOGGER_LEVEL);
			
			LOG.info("Plugin de despliegue y configuracion del artefacto en JBoss Fuse.");
			LOG.info("Cristian Saez V.");

			Options.load(args);
			install.init();

		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		}

	}

	public void init() throws Exception {

		/**
		 * Preguntar si el bundle ya existe
		 */
		ProfileEdit profileEdit = new ProfileEdit();
		Map<String, String> artifactMap = profileEdit.bundleExist();
		
		if (artifactMap != null) {
			profileEdit.bundleDelete(artifactMap);
		}
		
		ProfileProperties profileProperties = new ProfileProperties();
		String cmdProperties = profileProperties.build();
		
		String cmdBundleInstall = profileEdit.cmdBundleInstall();
		
		Shell shell = new Shell();
		shell.execFuse(cmdProperties, cmdBundleInstall);
		
	}

}
