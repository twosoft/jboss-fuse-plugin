package cl.twosoft.jenkins.plugin.fuse;

/**
 * Twosoft 2018
 * @author Cristian Saez V.
 *
 */
public final class Logger {

	private String className;
	private static String level;

	public static final Logger factory(Class<?> clazz) {
		return new Logger(clazz, Logger.level);
	}

	public static final void setLevel(String level) {
		Logger.level = level;
	}

	public Logger(Class<?> clazz, String level) {
		this.className = clazz.getSimpleName();
		Logger.level = level;
	}

	private void print(String msg, String level) {
		String message = this.template(msg, level);
		System.out.println(message);
	}

	private String template(String level, String msg) {
		
		String objectName = "";
		if (LEVEL.DEBUG.equals(Logger.level))
			objectName = this.className;
		String message = "[" + level + "]" + objectName + "-> " + msg;
		return message;
	}

	public void info(String msg) {
		this.print(LEVEL.INFO, msg);
	}

	public void warn(String msg) {
		this.print(LEVEL.WARN, msg);
	}

	public void debug(String msg) {
		if (LEVEL.DEBUG.equals(Logger.level))
			this.print(LEVEL.DEBUG, msg);
	}

	public void error(String msg) {
		this.print(LEVEL.ERROR, msg);
	}

	public final class LEVEL {

		private LEVEL() {
		}

		public static final String INFO = "INFO";
		public static final String WARN = "WARN";
		public static final String DEBUG = "DEBUG";
		public static final String ERROR = "ERROR";

	}

}
