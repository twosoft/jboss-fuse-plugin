package cl.twosoft.jenkins.plugin.fuse;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Twosoft 2018
 * @author Cristian Saez V.
 *
 */
public class ProfileProperties {

	private static Logger LOG = Logger.factory(ProfileProperties.class);

	private static final String PROP_FOLDER = "/files/properties/";

	public ProfileProperties() {
	}

	/**
	 * @author Cristian Saez V.
	 * @return {@link String} (comand)
	 * @throws Exception
	 */
	public String build() throws Exception {

		return this.init();

	}

	private String init() throws Exception {

		Properties props = this.open();
		if (props == null)
			return null;

		@SuppressWarnings("unchecked")
		Enumeration<String> propsKeys = (Enumeration<String>) props.propertyNames();
		String[] propsCmd = new String[props.size()];
		int i = 0;

		LOG.info("Creando las propiedades del artefacto para Fuse...");

		while (propsKeys.hasMoreElements()) {
			String key = (String) propsKeys.nextElement();
			propsCmd[i] = this.create(key, props.getProperty(key));
			LOG.debug(propsCmd[i]);
			i++;
		}

		LOG.info("Propiedades del artefacto creadas...");

		return this.implode(propsCmd);

	}

	private String create(String key, String value) {
		
		String cmdProfileProperty = PropertyApp.get(PropertyApp.PROFILE_EDIT_PROPERTIES);
		cmdProfileProperty = cmdProfileProperty.replaceAll(EnvVar.token(EnvVar.PROPERTY_NAME), key);
		cmdProfileProperty = cmdProfileProperty.replaceAll(EnvVar.token(EnvVar.PROPERTY_VALUE), value);
		cmdProfileProperty = cmdProfileProperty.replaceAll(EnvVar.token(EnvVar.ARTIFACT_ID), Options.getArtifactId());
		cmdProfileProperty = cmdProfileProperty.replaceAll(EnvVar.token(EnvVar.PROFILE_PROPERTIES), Options.getProfileProperties());
		
		return cmdProfileProperty;
		
	}

	/**
	 * @author Cristian Saez V.
	 * @return
	 * @throws Exception
	 */
	private Properties open() throws Exception {

		LOG.info("Buscando las propiedades del artefacto");
		
		try {

			String path = this.propertiesPath();
			LOG.debug("Buscando properties en: " + path);
			Properties properties = new Properties();
			properties.load(new FileReader(path));

			LOG.info("Propiedades del artefacto encontradas");

			return properties;

		} catch (FileNotFoundException e) {
			LOG.info("No se encontraron propiedades para configurar");
			return null;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw e;
		}

	}

	/**
	 * @author Cristian Saez V.
	 * @return
	 */
	private String propertiesPath() {
		return Options.getWorkspace() + PROP_FOLDER + Options.getArtifactId() + ".properties";
	}
	
	/**
	 * @author Cristian Saez V.
	 * @param array
	 * @return
	 */
	private String implode(String[] array) {
		
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			stringBuffer.append(array[i]).append("\n");
		}
		
		return stringBuffer.toString();
		
	}

}
