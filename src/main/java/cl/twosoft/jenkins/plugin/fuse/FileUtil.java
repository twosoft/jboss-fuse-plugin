package cl.twosoft.jenkins.plugin.fuse;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.Properties;

/**
 * Twosoft 2018
 * @author Cristian Saez V.
 *
 */
public class FileUtil {

	private static final Logger LOG = Logger.factory(FileUtil.class);
	
	public FileUtil() {

	}

	/**
	 * @author Cristian Saez V.
	 * @param cmd
	 * @return
	 * @throws Exception
	 */
	public String write(String cmd) throws Exception {

		BufferedWriter bufferedWriter = null;
		String script = "/tmp/fuse-script";
		bufferedWriter = new BufferedWriter(new FileWriter(script));
		bufferedWriter.write(cmd);

		bufferedWriter.close();

		return script;

	}

	/**
	 * @author Cristian Saez V.
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public Properties properties(String path) throws Exception {
		try {

			InputStream inputStream = new FileInputStream(path);
			Properties properties = new Properties();
			properties.load(inputStream);

			return properties;

		} catch (FileNotFoundException e) {
			return null;
		} catch (Exception e) {
			LOG.error("Ocurrio un error al tratar de leer las propiedades de: " + path);
			throw e;
		}
	}
	
	public Properties localProperties(String path) throws Exception {
		try {

			LOG.debug("Cargando propiedades...");

			InputStream url = this.getClass().getClassLoader().getResourceAsStream(path);
			Properties properties = new Properties();
			properties.load(url);
			
			LOG.debug("Propiedades OK");
			
			return properties;

		} catch (Exception e) {
			LOG.error("Error al cargar propiedades de: " + path);
			LOG.error(" - " + e.getMessage());
			throw e;
		}
	}

}
